def getInstrumentProperties():
	
	instrumentProperties = {
			
			'duts_availability': [1,0,0,0],
			'duts_IP': ["10.10.10.42","10.10.10.42","10.10.10.42","10.10.10.42"],
			'duts_slot': [8,8,8,8],
			'duts_channels': [0,1,2,3],
			
			'duts_calibration': True,
			'duts_verification': True,

			'minWav': 1.527605e-6,
			'maxWav': 1.568773e-6,
			'wavePoints': 5,
			'minPower': 7.5,
			'maxPower': 13.5,
			'powerPoints': 5,
			
			'pm_type': "VIAVI", # "EXFO"
			'pm_availability': [1,0,0,0],
			'pm_IP': ["10.10.10.177","10.10.10.177","10.10.10.177","10.10.10.177"],
			'pm_slot': [0,0,0,0], 
			'pm_channel': [0,1,2,3],

			'pmTavg': 1,						# seconds of averaging on external pm (it will be used for offset calculation)
			'tSettle': 30.0,			        # seconds between samples. This must include time for the input to settle plus the power meter averaging time
			
			}
	return instrumentProperties

if __name__ == "__main__":
	instrumentProperties = getInstrumentProperties()
	print instrumentProperties
