#!/usr/bin/python

# OE average power calibration for OEBlade

import sys
sys.path.insert(0, r'..\CSPY')
sys.path.insert(0, r'..\gipy')

from gipy.exfoPM import EXFOPM
from gipy.viaviPM import VIAVIPM

import vxi11

#Misc
import subprocess
from time import sleep,time
import os
import glob
import traceback
import json
import smtplib
from cspy.csi import CSI
import uitlaInstrumentsSetup

#numpy
import numpy
from numpy import *
import serialize_json as sj
import numpy.polynomial.polynomial as poly
from pylab import savefig
from matplotlib.pyplot import *

from argparse import ArgumentParser
import logging

# misc
import shutil
SPEED_OF_LIGHT = 299792458.0	#m/s

class uitlaCalibration():

	def setupInstruments(self):
		ready = True
		instrumentProperties = uitlaInstrumentsSetup.getInstrumentProperties()
			
		instruments = {}
	
		pms = []
		try:
			if instrumentProperties['pm_type'] == "VIAVI":
				for index, pm_Ip in enumerate(instrumentProperties['pm_IP']):
					if (instrumentProperties['pm_availability'][index] == 1):
						try:
							pm = VIAVIPM(ip=pm_Ip).blades[instrumentProperties['pm_slot'][index]].channels[instrumentProperties['pm_channel'][index]]
							pm.averaging_time = instrumentProperties['pmTavg']
							pms += [pm]
						except:
							print "The defined exfo pm in slot {} cannot be found \n".format(instrumentProperties['pm_slot'][index])
							ready = False
				if 	pms.__len__() == 0:	
					ready = False
				instruments['pms'] = pms
			else:
				if instrumentProperties['pm_type'] == "EXFO":
					for index, pm_Ip in enumerate(instrumentProperties['pm_IP']):
						if (instrumentProperties['pm_availability'][index] == 1):
							try:
								pm = EXFOPM(ip=pm_Ip,slot=instrumentProperties['pm_slot'][index])
								pm.averaging_time = instrumentProperties['pmTavg']
								pms += [pm]
							except:
								print "The defined exfo pm in slot {} cannot be found \n".format(instrumentProperties['pm_slot'][index])
								ready = False
					if 	pms.__len__() == 0:	
						ready = False
					instruments['pms'] = pms
				
			for pm in pms:
				pm.averaging = instrumentProperties['pmTavg']
				
		except:
			print "Could not find EXFO or VIAVI PM. Ensure it is turned on and is plugged in\n" 
			ready = False
		
		duts = []
		for index, dut_Ip in enumerate(instrumentProperties['duts_IP']):
			if (instrumentProperties['duts_availability'][index] == 1):
				dut_instr = CSI(ip=dut_Ip)
				dut_instr_blade = dut_instr.blades[instrumentProperties['duts_slot'][index]]
				dut = dut_instr_blade.channels[instrumentProperties['duts_channels'][index]]
				dut.ip = dut_Ip
				dut.slot = instrumentProperties['duts_slot'][index]
				dut.channel = index+1

				duts += [dut]
				self.serial = dut.blade.serial
				self.dut_ip = dut_Ip
				self.dut_slot = instrumentProperties['duts_slot'][index]
				#Checking if there is a serial number folder, and creating one if there is not.
				if os.path.isdir(self.serial) == False:
					os.mkdir(self.serial)
				if instrumentProperties['duts_calibration'] == True:
					os.system("\"C:\\Program Files (x86)\\Coherent Solutions\\CSLServer\\bset.exe\" -b%d -p settings.channel[%d].power.power_correction -w [0,0] " %(self.dut_slot, index))
		
		if instrumentProperties['duts_calibration'] == True:
			os.system("\"C:\\Program Files (x86)\\Coherent Solutions\\CSLServer\\bset.exe\" -b%d -c" %(self.dut_slot))
			os.system("\"C:\\Program Files (x86)\\Coherent Solutions\\CSLServer\\bset.exe\" -b%d -r" %(self.dut_slot))
			sleep(10)
		instruments['duts'] = duts
		self.instruments = instruments

	def convertNMtoTHZ(self,wavelength):
		return ((SPEED_OF_LIGHT)/(wavelength*1e12));
	
	def powerScan(self):
		instrumentProperties = uitlaInstrumentsSetup.getInstrumentProperties()
		
		minWav = instrumentProperties['minWav']
		maxWav = instrumentProperties['maxWav']
		wavePoints = instrumentProperties['wavePoints']
		waveList = numpy.linspace(minWav, maxWav, wavePoints)
		
		minPower = instrumentProperties['minPower']
		maxPower = instrumentProperties['maxPower']
		powerPoints = instrumentProperties['powerPoints']
		powerList = numpy.linspace(minPower, maxPower, powerPoints)
		
		results = []
		scans = []
		
		for wavIndex, wavelength in enumerate(waveList):			
			readPowerList = []
			offset = []
			print "Starting scan at wavelength: " + str(wavelength) + " nm"
			
			for dut in self.instruments['duts']:
				dut.wavelength = wavelength
			for pm in self.instruments['pms']:
				pm.wavelength = wavelength*1e9
			
			for dut in self.instruments['duts']:
				dut.power = 0
				dut.state = 1
			
			for dut in self.instruments['duts']:
				while not dut.wavelength['lock']:
					sleep(1)


			offset = {}
			set_power = {}
			read_power = {}
			for pm_index, pm in enumerate(self.instruments['pms']):
				offset[pm_index] = []
				set_power[pm_index] = []
				read_power[pm_index] = []
				
			for powerIndex, power in enumerate(powerList): 
				
				for dut_index, dut in enumerate(self.instruments['duts']):
					try:
						dut.power = power
					except:
						raise ValueError("Cannot set the power to %f" % power)
						
					while(abs(dut.power['act'] - power) > 0.01):
						sleep(1)
				
				for pm_index, pm in enumerate(self.instruments['pms']):
					offset[pm_index] += [float(pm.power['act'])-power]
					set_power[pm_index] += [float(power)]
					read_power[pm_index] += [float(pm.power['act'])]
			
			results += [[self.convertNMtoTHZ(wavelength), offset]]
			scans +=[{"wavelength":wavelength,"pow_set":set_power,"pow_act":read_power,"offset":offset}]
		
		if instrumentProperties['duts_calibration'] == True:
			offset_array = {}
			for dut_index, dut in enumerate(self.instruments['duts']):
				freq_array = []
				new_offset_array = []
				offset_array[dut_index] = []
				for wavIndex, wavelength in enumerate(waveList):			
					offset_array[dut_index] += [results[wavIndex][1][dut_index]]
					freq_array += [results[wavIndex][0]]
				for powerIndex, power in enumerate(powerList):
					new_offset_array += [mean(array(offset_array[dut_index])[:,powerIndex])]	
				w = polyfit(freq_array, new_offset_array,1)
				data = {"serial":self.serial,'calibration':{"scans":scans},'coeff':[float(w[0]),float(w[1])]}
				f = self.serial + "\Laser_calibration.dat"
				s = sj.data_to_json(data,indent=4)
				open(f, 'w').write(s)
				#f.close()
				os.system("\"C:\\Program Files (x86)\\Coherent Solutions\\CSLServer\\bset.exe\" -b%d -p settings.channel[%d].power.power_correction -w [%f,%f] " %(self.dut_slot, dut_index, w[0],w[1]))
			os.system("\"C:\\Program Files (x86)\\Coherent Solutions\\CSLServer\\bset.exe\" -b%d -c" %(self.dut_slot))
			os.system("\"C:\\Program Files (x86)\\Coherent Solutions\\CSLServer\\bset.exe\" -b%d -r" %(self.dut_slot))
			sleep(10)
		
		if instrumentProperties['duts_verification'] == True:
			data = {"serial":self.serial,'verification':{"scans":scans}}
			f = self.serial + "\Laser_calibration.dat"
			s = sj.data_to_json(data,indent=4)
			open(f, 'w').write(s)
		
		for dut in self.instruments['duts']:
			dut.state = 0

cal = uitlaCalibration()
cal.setupInstruments()
cal.powerScan()